package com.javangular.userportal.controller;

import com.javangular.userportal.model.CustomerM;
import com.javangular.userportal.repo.CustomerMongoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Null;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins= "localhost:4200")
@RestController
@RequestMapping("/api")
public class CustomerController {

    @Autowired
    CustomerMongoRepo customerMongoRepo;

    @GetMapping("/customers")
    public List<CustomerM>getAllCustomers(){
        System.out.println("Get all customers");
        return customerMongoRepo.findAll();
    }

    @PostMapping("customers/create")
    public CustomerM createCustomer(@Valid @RequestBody CustomerM customer){
        System.out.println("Create customer: " + customer.getName() + "...");
        customer.setActive(false);
        return customerMongoRepo.save(customer);
    }

    @PutMapping("customers/{id}")
    public ResponseEntity<CustomerM> updateCustomer(@PathVariable("id") String id, @RequestBody CustomerM customer){
        System.out.println("Update Customer with ID = " + id + "...");

        CustomerM customerData = customerMongoRepo.findById(id).orElse(null);
        if (customer == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        customerData.setName(customer.getName());
        customerData.setAge(customer.getAge());
        customerData.setActive(customer.isActive());

        CustomerM updatedCustomer = customerMongoRepo.save(customerData);
        return new ResponseEntity<>(updatedCustomer, HttpStatus.OK);
    }

    @DeleteMapping("customers/{id}")
    public ResponseEntity<String> deleteCustomer(@PathVariable("id") String id){
        System.out.println("Delete Customer with ID = " + id + "...");

        customerMongoRepo.deleteById(id);

        return new ResponseEntity<>("Customer has been deleted!", HttpStatus.OK);
    }

    @DeleteMapping("customers/delete")
    public ResponseEntity<String> deleteAllCustomers() {
        System.out.println("Delete All Customers...");
        customerMongoRepo.deleteAll();

        return new ResponseEntity<>("All customers have been deleted!", HttpStatus.OK);
    }


}
