import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {CreateCustomerComponent} from './customers/create-customer/create-customer.component';
import {CustomerDetailsComponent} from './customers/customer-details/customer-details.component';
import {CustomersListComponent} from './customers/customers-list/customers-list.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {CustomerService} from "./customers/customer.service";

@NgModule({
    declarations: [
        AppComponent,
        CreateCustomerComponent,
        CustomerDetailsComponent,
        CustomersListComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule
    ],
    providers: [CustomerService],
    bootstrap: [AppComponent]
})
export class AppModule {}
