package com.javangular.userportal.model;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "customer")
public class CustomerM {

    @Id
    private String id;

    private String name;
    private int age;
    private boolean active;

    public CustomerM() {
    }

    public CustomerM(String name, int age) {
        this.name = name;
        this.age = age;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Customer [id= " + id + ", name= " + name + ", age=" + age + ", active= " + active + "]";
    }
}
