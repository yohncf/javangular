package com.javangular.userportal.repo;

import com.javangular.userportal.model.CustomerM;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerMongoRepo extends MongoRepository<CustomerM, String>{
}

